import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import CurriculumVitaeMain from '../components/CurriculumVitaeMain'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Sorin PUNCEA CV',
      component: CurriculumVitaeMain
    }
  ]
})
